## Events Schedule

## 📜 Description
This project originated from a React course, serving as a practical exercise to reinforce skills in React Hooks.
It contains two projects:
- A React frontend;
- A dummy node backend, that React can send and fetch data;
This application has some pages used to view, create, edit and delete some events.

## 🖼️ Screenshots
I'll add some prints to show how this project looks.

### Login Screen
![LoginScreen](./frontend/public/LoginScreen.png)

### Events Screen
![EventsScreen](./frontend/public/EventsScreen.png)

### Event Detail Screen
![EventDetailScreen](./frontend/public/EventDetailScreen.png)

### Edit Event Screen
![EditEventScreen](./frontend/public/EditEventScreen.png)


## 💻 Requirements
You will need:
* npm;
* react;
* node;

## 🔨 Installation 
To install this project in your local machine, follow this steps:

Create a folder in your local machine.
Then run

`git clone {this project link}`

Go to frontend project folder.

`npm install`

Go to backend project folder.

`npm install`


## ☕ Using Project
To use this project, you need to start both frontend and backend applications.

Start by the backend, go to project folder, which has te package.json and run:

`npm start`

Now go to frontend folder, and run:

`npm start`

You MUST have both projects running for the projects work.

## 🔧 Fixes and new features
This project is under development and the next steps are:
- Better sign in and sign up screen;
- Improve layout
- ...

## 🙂 Author
[@bhtrevisan](https://gitlab.com/bhtrevisan)
