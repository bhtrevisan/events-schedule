// Challenge / Exercise

// 1. Add five new (dummy) page components (content can be simple <h1> elements)
//    - HomePage
//    - EventsPage
//    - EventDetailPage
//    - NewEventPage
//    - EditEventPage
// 2. Add routing & route definitions for these five pages
//    - / => HomePage
//    - /events => EventsPage
//    - /events/<some-id> => EventDetailPage
//    - /events/new => NewEventPage
//    - /events/<some-id>/edit => EditEventPage
// OK 3. Add a root layout that adds the <MainNavigation> component above all page components
// OK 4. Add properly working links to the MainNavigation
// OK 5. Ensure that the links in MainNavigation receive an "active" class when active
// 6. Output a list of dummy events to the EventsPage
//    Every list item should include a link to the respective EventDetailPage
// 7. Output the ID of the selected event on the EventDetailPage
// BONUS: Add another (nested) layout route that adds the <EventNavigation> component above all /events... page components
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import RootLayout from "./components/pages/Root";
import HomePage from "./components/pages/Home";
import EditEventPage from "./components/pages/EditEvent";
import EventsPage, { loader as eventsLoader } from "./components/pages/Events";
import EventDetailPage, { loader as eventDetailLoader, action as deleteEventAction } from "./components/pages/EventDetail";
import NewEventPage from "./components/pages/NewEvent";
import EventRootLayout from "./components/pages/EventsRoot";
import ErrorPage from "./components/pages/Error";
import { action as manipulateEventAction } from "./components/EventForm";
import NewsletterPage, { action as newsletterAction } from "./components/pages/Newsletter";
import AuthenticationPage, { action as authAction } from "./components/pages/Authentication";
import { action as logoutAction } from './components/pages/Logout';
import { tokenLoader, checkAuthLoader } from './components/utils/auth';



const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    id: 'root',
    loader: tokenLoader,
    children: [
      { path: '/', element: <HomePage /> },
      { path: 'auth', element: <AuthenticationPage />, action: authAction },
      {
        path: '/events',
        element: <EventRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader
          },
          {
            path: ':eventId',
            id: 'event-detail',
            loader: eventDetailLoader,
            children: [
              {
                index: true,
                element: <EventDetailPage />,
                action: deleteEventAction,
              },
              {
                path: 'edit',
                element: <EditEventPage />,
                action: manipulateEventAction,
                loader: checkAuthLoader,
              }
            ]

          },
          {
            path: 'new',
            element: <NewEventPage />,
            action: manipulateEventAction,
            loader: checkAuthLoader,
          },
        ],
      },
      { path: 'newsletter', element: <NewsletterPage />, action: newsletterAction },
      { path: 'logout', action: logoutAction }
    ]
  }
])


function App() {
  return <RouterProvider router={router} />
}

export default App;
