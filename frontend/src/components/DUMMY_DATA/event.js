const dummyEvent = [
    { id: 'event1', content: 'Math', title: 'Math Class', date: '18-10' },
    { id: 'event2', content: 'English', title: 'English Class', date: '19-10' },
    { id: 'event3', content: 'Portuguese', title: 'Portuguese Class', date: '18-10' },
    { id: 'event4', content: 'Physics', title: 'Physics Class', date: '21-10' },
    { id: 'event5', content: 'Chemistry', title: 'Chemistry Class', date: '20-10' },
];

export {dummyEvent};