import NewsletterSignup from '../NewsletterSignup';
import PageContent from './PageContent';

function NewsletterPage() {
  return (
    <PageContent title="Join our awesome newsletter!">
      <NewsletterSignup />
    </PageContent>
  );
}

export default NewsletterPage;

export async function action({ request }) {
  const data = await request.formData();
  const email = data.get('email');

  // send to backend newsletter server ...
  if (email) {
    return { message: 'Signup successful!' };
  }
  else {
    return { message: 'Email is empty' };
  }
}