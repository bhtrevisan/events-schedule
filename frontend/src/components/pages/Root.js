import { Outlet, useLoaderData, useSubmit } from "react-router-dom";
import MainNavigation from "../MainNavigation";
import { useEffect } from "react";
import { getTokenDuration } from "../utils/auth";

function RootLayout() {
  const token = useLoaderData();
  const submit = useSubmit();
  const timeout = 1 * 60 * 60 * 1000; // 1 hour in mili seconds

  useEffect(() => {
    if (!token)
      return;

    if (token === 'EXPIRED') {
      submit(null, { action: '/logout', method: 'post' })
      return;
    }

    const tokenDuration = getTokenDuration();

    setTimeout(() => {
      submit(null, { action: '/logout', method: 'post' })
    }, tokenDuration)

  }, [token, submit]);

  return (
    <>
      <MainNavigation />
      <main>
        <Outlet />
        {/* {navigation.state === 'loading' && <p>Loading...</p>} */}
      </main>
    </>
  )
}

export default RootLayout;